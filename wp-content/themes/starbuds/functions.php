<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	return;
}

Timber::$dirname = array('templates', 'views');

class LumberSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'register_menus' ) );
		parent::__construct();
	}

	function register_menus() {
	  register_nav_menus(
	    array(
	      'header-menu' => __( 'Header Menu' )
	    )
	  );
	}

	function register_post_types() {
		register_post_type( 'locations',
			array(
				'labels' => array(
					'name' => __( 'Locations' ),
					'singular_name' => __( 'Location' ),
					'edit_item' => __('Edit Location'),
					'add_new_item'       => __( 'Add New Location'),
				),
				'public' => true,
				'publicly_queryable' => true,
				'has_archive' => false,
				'menu_position' => 3,
				'hierarchical' => true,
				'show_in_rest' => true,
				'supports' => array(
					'title', 'page-attributes'
				),
				'menu_icon' => 'dashicons-screenoptions',
				'rewrite' => array('slug' => 'location'),
			)
		);
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu('header-menu');
		$context['site'] = $this;

		$context['profiles'] = get_field('profiles', 'options');
		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new LumberSite();

//post pagination
function blog_query( $query ) {
  if ( $query->is_main_query() && !is_admin() && is_home()) {
    $query->set( 'post_type', array( 'post' ));
		$query->set( 'posts_per_page',get_option( 'posts_per_page' ));
  }
}
add_action( 'pre_get_posts', 'blog_query' );

//enqueue scripts and styles safely
function lumber_styles_scripts() {
	wp_enqueue_style('vendor', get_template_directory_uri().'/dist/css/vendor/vendor.css', array(), filemtime( get_template_directory().'/dist/css/vendor/vendor.css' ));
	wp_enqueue_style('main', get_template_directory_uri().'/dist/css/main.css', array(), filemtime( get_template_directory().'/dist/css/main.css' ));
	wp_enqueue_script('vendor-js', get_template_directory_uri().'/dist/js/vendor/vendor.js', array(), filemtime( get_template_directory().'/dist/js/vendor/vendor.js' ), true);
	wp_enqueue_script('main-js', get_template_directory_uri().'/dist/js/app.js', array('jquery'), filemtime( get_template_directory().'/dist/js/app.js' ), true);
}
add_action( 'wp_enqueue_scripts', 'lumber_styles_scripts' );

//deregister cf7 styling
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
function wps_deregister_styles() {
    wp_deregister_style( 'contact-form-7' );
}

//add options page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

//Filtering a Class in Navigation Menu Item
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
 if(is_single() && $item->title == "Locations"){
 	$classes[] = 'current-menu-item';
 }
 return $classes;
}

//Custom login page
add_filter('login_message', 'login_message_update');
function login_message_update() {
	return '<div class="members">For franchisees, log in at <a href="//members.starbudsnation.ca/login">members.starbudsnation.ca/login</a></div><style>.members{background:#f0bb1a;padding:1rem;color:#fff;text-align:center;border-radius:3px;}</style>';
}
