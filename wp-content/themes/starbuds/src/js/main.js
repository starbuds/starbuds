/* global jQuery */

// Init ScrollMagic
var controller = new ScrollMagic.Controller();

jQuery(function ($) {
  $('body').addClass('loaded')

  //simple animaion delays
  $('.anim').each(function() {
    $(this).css('animation-delay', (Math.random() * 3 - 0.3).toString() + 's')

    var parent = $(this).closest('section')[0]
      $(this).css('transition', (Math.random() * 3+1).toString() + 's')
      .css('transition-delay', (Math.random()).toString() + 's')[0]
		var ourScene = new ScrollMagic.Scene({
			triggerElement: parent,
      triggerHook: 0.5
		})
		.setClassToggle(this, 'anim--fade-in')
		.addTo(controller);
  })

  //simple text animations
  $('.text-anim').each(function() {

		var ourScene = new ScrollMagic.Scene({
			triggerElement: this,
      triggerHook: 0.7
		})
		.setClassToggle(this, 'text-anim--fade-in')
		.addTo(controller);
  })

  //benefits
  $('.benefit-list .benefit').each(function() {
    $(this).css('left', (Math.random() * 4).toString()+'rem')
  })

  //franchise values
  var values = [];
  $('.fact__value').each(function(){
    if($(this).text().indexOf('.') === -1) {
      // values.push(new CountUp($(this)[0], 0, parseFloat($(this).text())))
    } else {
      // values.push(new CountUp($(this)[0], 0, parseFloat($(this).text()), 1))
    }
  })

  //franchise form fix
  var $hidden = $('.franchise .wpcf7-form').children().first()
  $('.franchise .wpcf7-form').append($hidden)

  $(values).each(function(){
    // $(this)[0].start();
  })

})

//Modal Window
jQuery( function ($) {
  if (Cookies.get('isAdult') !== 'true') {
    $('.verify').show()
  }
})

function enterSite() {
  Cookies.set('isAdult', true)
  if(Cookies.get('isAdult') === 'true') {
    jQuery('.verify').fadeOut()
  }
}
