<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['locations'] = Timber::get_posts(array(
  'post_type' => 'locations',
  'orderby' => 'menu_order',
  'order' => 'ASC',
  'posts_per_page' => '-1'
));


$location_data = '<?xml version="1.0" encoding="utf-8"?><markers>';

// WP_Query arguments
$args = array(
  'post_type' => 'locations',
  'orderby' => 'title',
  'order' => 'ASC',
  'posts_per_page' => '-1'
);

// The Query
$location_query = new WP_Query( $args );
$count = 0;
// The Loop
if ( $location_query->have_posts() ) {
	while ( $location_query->have_posts() ) {
		$location_query->the_post();
    $location_data .= '<marker name="'.get_the_title().'" lat="'.get_field("lat").'" lng="'.get_field("lng").'" category="" address="'.get_field("address").'" address2="'.get_field("address2").'" city="'.get_field("city").'" state="'.get_field("state").'" postal="'.get_field("postal").'" country="'.get_field("country").'" phone="'.get_field("phone").'" email="'.get_field("email").'" web="'.get_the_permalink().'" hours1="" hours2="" hours3="" featured="" features="" />';
	}
} else {
	// no posts found
}

$location_data .= '</markers>';

$context['location_data'] = json_encode($location_data);

// Restore original Post Data
wp_reset_postdata();

Timber::render('page-locations.twig', $context );
